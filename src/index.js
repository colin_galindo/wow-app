import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';

import App from './components/App/App.js';
import registerServiceWorker from './utils/registerServiceWorker.js';
import { createStore } from './store/createStore.js';
import './index.css';

const history = createBrowserHistory();
const store = createStore({}, history);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
