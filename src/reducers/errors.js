import createReducer from '../utils/createReducer';
import { actions as characterActions } from '../actions/character';
import { actions } from '../actions/errors';

const initialState = [];

const actionHandlers = {
  [characterActions.FETCH_CHARACTERS_ERROR](state, { payload }) {
    return state.concat(payload.error);
  },
  [actions.CLEAR_ERRORS](state, { payload }) {
    return initialState;
  },
};

export default createReducer(actionHandlers, initialState);
