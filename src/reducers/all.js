import characters from './characters';
import errors from './errors';

const reducers = { characters, errors };

export default reducers;
