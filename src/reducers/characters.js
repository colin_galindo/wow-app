import createReducer from '../utils/createReducer';
import { actions } from '../actions/character';

const initialState = { hasError: false, characters: [] };

function bossesKilled(raid, difficulty) {
  return raid.bosses.reduce((bossCounter, boss) => {
    if (boss[`${difficulty}Kills`] > 0) {
      return bossCounter + 1;
    }
    return bossCounter;
  }, 0);
}

function normalizeCharacter(character) {
  const uldir = character.progression.raids.find(raid => raid.name === 'Uldir');

  return {
    uldir: {
      normalProgress: `${bossesKilled(uldir, 'normal')}/${uldir.bosses.length}`,
      heroicProgress: `${bossesKilled(uldir, 'heroic')}/${uldir.bosses.length}`,
      ...uldir,
    },
    ...character,
  };
}

const actionHandlers = {
  [actions.FETCH_CHARACTERS_SUCCESS](state, { payload }) {
    return payload.map(normalizeCharacter);
  },
};

export default createReducer(actionHandlers, initialState);
