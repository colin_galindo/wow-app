import FetchUtils from '../utils/fetchUtils';

const actions = {
  FETCH_CHARACTERS: 'FETCH_CHARACTERS',
  FETCH_CHARACTERS_SUCCESS: 'FETCH_CHARACTERS_SUCCESS',
  FETCH_CHARACTERS_ERROR: 'FETCH_CHARACTERS_ERROR',
};

const actionCreators = {
  fetchCharacters(characterNames) {
    return (dispatch) => {
      Promise.all(
        characterNames.map(character => FetchUtils.get(
          `https://us.api.battle.net/wow/character/Emerald%20Dream/${character}?fields=items&fields=progression&locale=en_US&apikey=dwpcu9w7jcs6359x42u5u4ane9v35cf2`,
        )),
      ).then((characters) => {
        dispatch(actionCreators.fetchCharactersSuccess(characters));
      }).catch((error) => {
        dispatch(actionCreators.fetchCharactersError(error));
      });
    };
  },

  fetchCharactersSuccess(characters) {
    return {
      type: actions.FETCH_CHARACTERS_SUCCESS,
      payload: characters,
    };
  },

  fetchCharactersError(error) {
    return {
      type: actions.FETCH_CHARACTERS_ERROR,
      payload: error,
    };
  },
};

export { actions, actionCreators };
