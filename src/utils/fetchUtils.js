/**
 * @namespace FetchUtils
 */

/**
 * Checks if there is a error status code and if so throws an error
 * @memberof FetchUtils
 * @private
 * @param {string} response The response object
 * @return {Object} Returns an object with the response
 * @throws Throws an error with the error from the response
 */
export function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Returns the Response json
 * @memberof FetchUtils
 * @private
 * @param {string} response The response object
 * @return {Object} Returns an object with the responseJSON
 */
function parseJSON(response) {
  return response.json();
}

/**
 * @memberof FetchUtils
 * @param {string} method This is the method of API call to send
 * @param {string} url The URL to request from
 * @param {Object} [options={}] Options to add to the request
 * @return {Promise} Returns a bluebird promise
 */
function makeFetchCall(method, url, options = {}) {
  const settings = Object.assign(
    {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
    },
    options,
  );

  if (method !== 'GET') {
    settings.body = JSON.stringify(settings.data);
    delete settings.data;
  }

  return fetch(url, settings)
    .then(checkStatus)
    .then(parseJSON);
}

export default {
  /**
   * Makes a GET fetch call to the given url with the given options
   * @function get
   * @example <caption>Example usage of FetchUtils.get</caption>
   * FetchUtils.get('/api/patient/1')
   * @memberof FetchUtils
   * @param {string} url The URL to request from
   * @param {Object} [options] Options to add to the request
   * @return {Promise} Returns a bluebird promise
   */
  get(url, options) {
    return makeFetchCall('GET', url, options);
  },

  /**
   * Makes a POST fetch call to the given url with the given options
   * @function post
   * @example <caption>Example usage of FetchUtils.post</caption>
   * FetchUtils.post('/api/patient/1', {data: { first_name: 'Colin'}})
   * @memberof FetchUtils
   * @param {string} url The URL to request from
   * @param {Object} [options] Options to add to the request
   * @return {Promise} Returns a bluebird promise
   */
  post(url, options) {
    return makeFetchCall('POST', url, options);
  },

  /**
   * Makes a PATCH fetch call to the given url with the given options
   * @function patch
   * @example <caption>Example usage of FetchUtils.patch</caption>
   * FetchUtils.patch('/api/patient/1', {data: { first_name: 'Colin'}})
   * @memberof FetchUtils
   * @param {string} url The URL to request from
   * @param {Object} [options] Options to add to the request
   * @return {Promise} Returns a bluebird promise
   */
  patch(url, options) {
    return makeFetchCall('PATCH', url, options);
  },

  /**
   * Makes a PUT fetch call to the given url with the given options
   * @function put
   * @example <caption>Example usage of FetchUtils.put</caption>
   * FetchUtils.put('/api/patient/1', {data: { first_name: 'Colin'}})
   * @memberof FetchUtils
   * @param {string} url The URL to request from
   * @param {Object} [options] Options to add to the request
   * @return {Promise} Returns a bluebird promise
   */
  put(url, options) {
    return makeFetchCall('PUT', url, options);
  },

  /**
   * Makes a DELETE fetch call to the given url with the given options
   * @function delete
   * @example <caption>Example usage of FetchUtils.delete</caption>
   * FetchUtils.delete('/api/patient/1')
   * @memberof FetchUtils
   * @param {string} url The URL to request from
   * @param {Object} [options] Options to add to the request
   * @return {Promise} Returns a bluebird promise
   */
  delete(url, options) {
    return makeFetchCall('DELETE', url, options);
  },
};
