import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';

import Header from '../Header/Header';
import { actionCreators as characterActionCreators } from '../../actions/character';
import './App.css';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

const characterNames = [
  'Bdn',
  'BigDrambus',
  'Messibain',
  'Oakshadon',
  'Sandural',
  'Schoolowheel',
  'Snellywop',
  'Sterunalive',
  'Velieliri',
  'zombeezywow',
];

function sortedCharacters(characters) {
  const newCharacters = characters.slice();
  newCharacters.sort((character1, character2) => {
    if (
      character1.items.averageItemLevelEquipped
      > character2.items.averageItemLevelEquipped
    ) {
      return -1;
    }
    if (
      character1.items.averageItemLevelEquipped < character2.items.averageItemLevelEquipped
    ) {
      return 1;
    }

    return 0;
  });

  return newCharacters;
}

class App extends Component {
  componentDidMount() {
    const { fetchCharacters } = this.props;
    fetchCharacters(characterNames);
  }

  render() {
    const { classes, characters } = this.props;

    return (
      <div className={classes.root}>
        <Header />
        {characters.length && (
          <Paper className={classes.paper}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>
                    <Typography>Character</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography>Item Level</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography>Uldir Normal</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography>Uldir Heroic</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {characters.map(character => (
                  <TableRow key={character.name}>
                    <TableCell>
                      <Typography>{character.name}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>
                        {character.items.averageItemLevelEquipped}
                      </Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>{character.uldir.normalProgress}</Typography>
                    </TableCell>
                    <TableCell>
                      <Typography>{character.uldir.heroicProgress}</Typography>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </Paper>
        )}
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  characters: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  errors: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  fetchCharacters: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  fetchCharacters: characterActionCreators.fetchCharacters,
};

function mapStateToProps(state) {
  const { characters, errors } = state;
  return {
    characters,
    errors,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));
