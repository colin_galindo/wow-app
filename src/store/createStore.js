import {
  createStore as reduxCreateStore,
  applyMiddleware,
  compose,
  combineReducers,
} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import Reducers from '../reducers/all.js';

/**
 * Initialize the redux store.
 *   - connects middleware and reducers
 *   - configures the chrome extension for redux
 */

/* istanbul ignore next */
function createStore(initialState = {}, history) {
  let composeEnhancers = compose;

  // Setup redux devtools chrome extension, if available and correct env
  if (typeof window !== 'undefined' && process.env.NODE_ENV !== 'production') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  }

  const createStoreWithMiddleware = composeEnhancers(
    applyMiddleware(thunk, routerMiddleware(history)),
  )(reduxCreateStore);

  /**
   * Connect reducers to the redux store.
   */
  const reducers = connectRouter(history)(combineReducers(Reducers));
  return createStoreWithMiddleware(reducers, initialState);
}

export { createStore };
